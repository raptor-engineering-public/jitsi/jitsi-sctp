#
# Regular cron jobs for the jitsi-sctp package
#
0 4	* * *	root	[ -x /usr/bin/jitsi-sctp_maintenance ] && /usr/bin/jitsi-sctp_maintenance
